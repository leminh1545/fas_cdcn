from __future__ import print_function, division
import os
import torch
import pandas as pd
#from skimage import io, transform
import cv2
import numpy as np
import random
import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
import pdb
import math
import os 


class Normaliztion_valtest(object):
    """
        same as mxnet, normalize into [-1, 1]
        image = (image - 127.5)/128
    """
    def __call__(self, sample):
        image_x, map_x, map_standard, spoofing_label = sample['image_x'],sample['map_x'],sample['map_standard'],sample['spoofing_label']
        new_image_x = (image_x - 127.5)/128     # [-1,1]
        new_map_x = map_x/255.0                 # [0,1]

        return {'image_x': new_image_x, 'map_x': new_map_x, 'map_standard': map_standard, 'spoofing_label': spoofing_label}


class ToTensor_valtest(object):
    """
        Convert ndarrays in sample to Tensors.
        process only one batch every time
    """

    def __call__(self, sample):
        image_x, map_x, map_standard, spoofing_label = sample['image_x'],sample['map_x'],sample['map_standard'],sample['spoofing_label']
        
        # swap color axis because    BGR2RGB
        # numpy image: (batch_size) x T x H x W x C
        # torch image: (batch_size) x T x C X H X W
        image_x = image_x[:,:,::-1].transpose((2, 0, 1))
        image_x = np.array(image_x)
                        
        map_x = np.array(map_x)
        
        return {'image_x': torch.from_numpy(image_x.astype(np.float)).float(), 'map_x': torch.from_numpy(map_x.astype(np.float)).float(), 'map_standard':torch.from_numpy(map_standard.astype(np.float)).float(),'spoofing_label': spoofing_label} 



class Spoofing_valtest(Dataset):

    def __init__(self, info_list,  transform=None):

        self.landmarks_frame = pd.read_csv(info_list)
        self.transform = transform

    def __len__(self):
        return len(self.landmarks_frame)

    
    def __getitem__(self, idx):
        image_path = str(self.landmarks_frame.iloc[idx, 1])
        map_path = str(self.landmarks_frame.iloc[idx, 4])
        spoofing_label = self.landmarks_frame.iloc[idx, 3]
        #change spoofing label
        if spoofing_label == 'real':
            spoofing_label = 1            # real
        else:
            spoofing_label = 0            # attack (fake)
            
        image_x, map_x, map_standard = self.get_single_image_x(image_path, map_path)
        #frequency_label = self.landmarks_frame.iloc[idx, 2:2+50].values  
        sample = {'image_x': image_x, 'map_x': map_x, 'map_standard': map_standard, 'spoofing_label': spoofing_label}

        if self.transform:
            sample = self.transform(sample)
        return sample

    def get_single_image_x(self, image_path, map_path):
        image_x = np.zeros((256, 256, 3))
        binary_mask = np.zeros((32, 32))
        map_x = np.ones((32, 32))

        image_x_temp = cv2.imread(image_path)
        image_x_temp_gray = cv2.imread(image_path, 0)

        image_x = cv2.resize(image_x_temp, (256, 256))
        image_x_temp_gray = cv2.resize(image_x_temp_gray, (32, 32))
        for i in range(32):
            for j in range(32):
                if image_x_temp_gray[i,j]>0:
                    binary_mask[i,j]=1
                else:
                    binary_mask[i,j]=0

        map_standard = np.ones((32,32))
        map_x_temp = cv2.imread(map_path, 0)
        map_x = cv2.resize(map_x_temp, (32, 32))
        temp = np.where(map_x < 1, map_x, 1)
        if(np.sum(temp) != 0):
            map_standard = temp          
        else:
            map_standard = binary_mask
            map_x = binary_mask
        return image_x, map_x, map_standard



if __name__ == '__main__':
    # usage
    # MAHNOB
    # root_list = '/wrk/yuzitong/DONOTREMOVE/BioVid_Pain/data/cropped_frm/'
    # trainval_list = '/wrk/yuzitong/DONOTREMOVE/BioVid_Pain/data/ImageSet_5fold/trainval_zitong_fold1.txt'
    
    val_list = "/home/minhtl/face_anti_spoof/Val.csv"
    BioVid_train = Spoofing_valtest(val_list, transform=transforms.Compose([Normaliztion_valtest(), ToTensor_valtest()]))
    
    dataloader = DataLoader(BioVid_train, batch_size=1, shuffle=False, num_workers=8)
    
    # print first batch for evaluation
    for i_batch, sample_batched in enumerate(dataloader):
        #print(i_batch, sample_batched['image_x'].size(), sample_batched['video_label'].size())
        # print(i_batch, sample_batched['image_x'],sample_batched['map_x'],sample_batched['map_standard'],sample_batched['spoofing_label'])
        # pdb.set_trace()
        map_standard = sample_batched['map_standard']
        map_sum = float(torch.sum(map_standard))
        if(map_sum == 0):
            print('have 0 mapsum', )
            break

            
 


