from __future__ import print_function, division
import torch
import argparse,os
import cv2
import numpy as np
import random
import math
import pdb
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

from models.CDCNs import Conv2d_cd, CDCNpp

import torch.nn.functional as F
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
from utils import AvgrageMeter, accuracy, performances_test
## load model detect face
protoPath = '/home/tlm/Documents/face_anti_spoof/livenessDetection/face_detector/deploy.prototxt'
modelPath = '/home/tlm/Documents/face_anti_spoof/livenessDetection/face_detector/res10_300x300_ssd_iter_140000.caffemodel'
net = cv2.dnn.readNetFromCaffe(protoPath, modelPath)
## face detection funtion
def face_detection(image_path):
    frame = cv2.imread(image_path)
    # frame = cv2.flip(frame, 0)
    (h, w) = frame.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
    net.setInput(blob)
    detections = net.forward()
    if len(detections) > 0:
    # we're making the assumption that each image has only ONE
    # face, so find the bounding box with the largest probability
        i = np.argmax(detections[0, 0, :, 2])
        confidence = detections[0, 0, i, 2]
        if confidence > 0.35:
            # compute the (x, y)-coordinates of the bounding box for
            # the face and extract the face ROI
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            face = frame[startY:endY, startX:endX]
    return face
## pre-process image funtion

def normaliztion(image_x):
    """
        same as mxnet, normalize into [-1, 1]
        image = (image - 127.5)/128
    """
    new_image_x = (image_x - 127.5)/128     # [-1,1]
    return new_image_x

def get_single_image_x(image_path):
    image_x = np.zeros((256, 256, 3))
    binary_mask = np.zeros((32, 32))

    image_x_temp = cv2.imread(image_path)
    image_x_temp_gray = cv2.imread(image_path, 0)

    image_x= cv2.resize(image_x_temp, (256, 256))
    image_x_temp_gray = cv2.resize(image_x_temp_gray, (32, 32))
    
    for i in range(32):
        for j in range(32):
            if image_x_temp_gray[i,j]>0:
                binary_mask[i,j]=1
            else:
                binary_mask[i,j]=0
    
    return image_x, binary_mask

def main():
    img_path = '/home/tlm/Downloads/VRA_dataset/test.jpeg'
    img_name = img_path.split('/')[-1]
    face_dir = '/home/tlm/Downloads/VRA_dataset/face_test'
    face_name = img_name
    face_path = os.path.join(face_dir, face_name)
    face_save_path= os.path.join(face_path, face_name)
    face = face_detection(img_path)
    cv2.imwrite(face_path, face)
    ## load model predict

    # os.environ["CUDA_VISIBLE_DEVICES"] = "%d" % (args.gpu)
    device = torch.device('cpu')
    
    #model = CDCNpp( basic_conv=Conv2d_cd, theta=0.7)
    model = CDCNpp( basic_conv=Conv2d_cd, theta=args.theta)

    model.load_state_dict(torch.load('CDCNpp_BinaryMask_P1_07/CDCNpp_BinaryMask_P1_07_60.pkl', map_location= device))

    # model = model.cuda()
    print(model) 
    model.eval()
 
    with torch.no_grad():

        image_x, binary_mask = get_single_image_x(face_path)
        image_x = normaliztion(image_x)

        # swap color axis because    BGR2RGB
        # numpy image: (batch_size) x T x H x W x C
        # torch image: (batch_size) x T x C X H X W
        image_x = image_x[:,:,::-1].transpose((2, 0, 1))
        image_x = np.array(image_x)          
        binary_mask = np.array(binary_mask)
        
        image_x_arr = torch.from_numpy(image_x.astype(np.float)).float() 
        binary_mask_arr = torch.from_numpy(binary_mask.astype(np.float)).float()
        image_x_arr = image_x_arr.unsqueeze_(0)
        image_x_arr.to(device)
        # test_data = Image_process(img_path, transform=transforms.Compose([Normaliztion(), ToTensor()]))
        # image_data = DataLoader(test_data, batch_size=1, shuffle=False, num_workers=1)
        # for sample_batched in image_data:

        map_score = 0.0

        map_x, embedding, x_Block1, x_Block2, x_Block3, x_input =  model(image_x_arr)

        score_norm = torch.sum(map_x)/torch.sum(binary_mask_arr)
        map_score += score_norm

        if map_score>1:
            map_score = 1.0

        print(map_score)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="save quality using landmarkpose model")
    parser.add_argument('--theta', type=float, default=0.7, help='hyper-parameters in CDCNpp')

    args = parser.parse_args()
    main()
