from __future__ import print_function, division
from pickle import STRING
import torch
# import matplotlib as mpl
# mpl.use('TkAgg')
# import matplotlib.pyplot as plt
import argparse,os
import pandas as pd
import cv2
import numpy as np
import random
import math
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms


from models.CDCNs import Conv2d_cd, CDCNpp
from models.DC_CDN_IJCAI21 import Conv2d_Hori_Veri_Cross, Conv2d_Diag_Cross, DC_CDN

from Loadtemporal_DepthMask_valtest import Spoofing_valtest, Normaliztion_valtest, ToTensor_valtest


import torch.nn.functional as F
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
import copy
import pdb
import time
from tqdm import tqdm
from utils import AvgrageMeter, accuracy, performances_test

# main function
def get_arguments():
    parser = argparse.ArgumentParser(description="test model")
    # parser.add_argument('--gpu', type=int, default=3, help='the gpu id used for predict')
    # parser.add_argument('--theta', type=float, default=0.7, help='hyper-parameters in CDCNpp')
    parser.add_argument('--csv', type=str, default="/home/minhtl/face_anti_spoof/Test.csv")
    parser.add_argument('--batch', type=int, default=1, help='batch size')
    parser.add_argument('--num_worker', type=int, default=4, help='num worker')


    return parser.parse_args()
def main():
    args = get_arguments()

    test_list = args.csv
    model_path = "CDCVpp_DepthMask_fullData_lr2e-4_b8_s30_e110/CDCVpp_DepthMask_fullData_lr2e-4_b8_s30_e110_31.pkl"
    test_name = test_list.split('/')[-1][:-4]
    model_name = model_path.split('/')[-1][:-4]
    model_dir = model_path.split('/')[0]


    os.environ["CUDA_VISIBLE_DEVICES"] = "2"
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    # device = torch.device('cpu')    
    
    model = CDCNpp( basic_conv=Conv2d_cd, theta=0.7)
    # model = nn.DataParallel(CDCNpp( basic_conv=Conv2d_cd, theta=0.7))
    # model = nn.DataParallel(DC_CDN(basic_conv1=Conv2d_Hori_Veri_Cross, basic_conv2=Conv2d_Diag_Cross, theta=0.8))

    model.load_state_dict(torch.load((model_path)))
    model = model.to(device)

    print(model) 
            # val for threshold
    test_data = Spoofing_valtest(test_list, transform=transforms.Compose([Normaliztion_valtest(), ToTensor_valtest()]))
    dataloader_test = DataLoader(test_data, batch_size=args.batch, shuffle=True, num_workers=args.num_worker)

    map_score_list = test(dataloader_test, model, device)

    map_score_test_filename = test_name + '({})'.format(model_name)
    map_score_test_path = os.path.join(model_dir, map_score_test_filename)
    with open(map_score_test_path, 'w') as file:
        file.writelines(map_score_list)    
    err_test, best_test_threshold, test_threshold_ACC, test_threshold_APCER, test_threshold_BPCER, test_threshold_ACER = performances_test(map_score_test_path)            
    print('Test:  ERR= %.4f (Best_threshold= %.4f) ACC= %.4f, APCER= %.4f, BPCER= %.4f, ACER= %.4f' % (err_test, best_test_threshold, test_threshold_ACC,
        test_threshold_APCER, test_threshold_BPCER, test_threshold_ACER))
    print('Finished testing')
def test(dataloader_test, model, device):
    model.eval()
    with torch.no_grad():
        map_score_list = []
        for i, sample_batched in tqdm(enumerate(dataloader_test)):
            inputs, spoofing_label = sample_batched['image_x'].to(device), sample_batched['spoofing_label'].to(device)
            val_map_x, val_map_standard = sample_batched['map_x'].to(device), sample_batched['map_standard'].to(device)

            # map_x, embedding, x_Block1, x_Block2, x_Block3, x_input =  model(inputs)
            map_x =  model(inputs)


            n = inputs.size(0)

            ## Compute map_score
            for j in range(len(inputs)):
                score_norm = torch.sum(map_x[j])/torch.sum(val_map_standard[j])
                if score_norm > 1:
                    score_norm = 1.0
                map_score_list.append('{} {}\n'.format(spoofing_label[j], score_norm))
    return map_score_list

if __name__ == "__main__":

    main()
