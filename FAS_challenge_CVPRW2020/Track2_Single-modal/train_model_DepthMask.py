from __future__ import print_function, division
import torch
import matplotlib as mpl
from torch._C import BenchmarkConfig
mpl.use('TkAgg')
import argparse,os
import pandas as pd
import cv2
import numpy as np
import random
import math
import shutil
import time
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from torch.utils.tensorboard import SummaryWriter
from models.DC_CDN_IJCAI21 import Conv2d_Hori_Veri_Cross, Conv2d_Diag_Cross, DC_CDN
# from models.CDCNs import Conv2d_cd, CDCNpp
from Loadtemporal_DepthMask_train import Spoofing_train, Normaliztion, ToTensor, RandomHorizontalFlip, Cutout, RandomErasing
from Loadtemporal_DepthMask_valtest import Spoofing_valtest, Normaliztion_valtest, ToTensor_valtest
from utils import AvgrageMeter, performances_val

import torch.nn.functional as F
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
import copy
import pdb

def get_arguments():
    parser = argparse.ArgumentParser(description="save quality using landmarkpose model")
    # parser.add_argument('--gpu', type=str, default="2, 3", help='the gpu id used for predict')
    parser.add_argument('--lr', type=float, default=0.0001, help='initial learning rate')  #default=0.0001
    parser.add_argument('--batchsize', type=int, default= 16, help='initial batchsize')  #default=7  
    # parser.add_argument('--numworker', type=int, default= 8, help='initial batchsize')  #default=7  
    parser.add_argument('--step_size', type=int, default=20, help='how many epochs lr decays once')  # 500  | DPC = 400
    parser.add_argument('--gamma', type=float, default=0.5, help='gamma of optim.lr_scheduler.StepLR, decay of lr')
    parser.add_argument('--echo_batches', type=int, default=50, help='how many batches display once')  # 50
    parser.add_argument('--epochs', type=int, default= 60, help='total training epochs')
    parser.add_argument('--log', type=str, default="CDCNpp_DepthMask_Data", help='log and save model name')
    parser.add_argument('--resume', type=str, default=None)
    parser.add_argument('--start-epoch', default=0, type=int, metavar='N', help='manual epoch number (useful on restarts)')
    # parser.add_argument('--theta', type=float, default=0.7, help='hyper-parameters in CDCNpp')
    return parser.parse_args()

def contrast_depth_conv(input):
    ''' compute contrast depth in both of (out, label) '''
    '''
        input  32x32
        output 8x32x32
    '''
    

    kernel_filter_list =[
                        [[1,0,0],[0,-1,0],[0,0,0]], [[0,1,0],[0,-1,0],[0,0,0]], [[0,0,1],[0,-1,0],[0,0,0]],
                        [[0,0,0],[1,-1,0],[0,0,0]], [[0,0,0],[0,-1,1],[0,0,0]],
                        [[0,0,0],[0,-1,0],[1,0,0]], [[0,0,0],[0,-1,0],[0,1,0]], [[0,0,0],[0,-1,0],[0,0,1]]
                        ]
    
    kernel_filter = np.array(kernel_filter_list, np.float32)
    
    kernel_filter = torch.from_numpy(kernel_filter.astype(np.float)).float().cuda()
    # weights (in_channel, out_channel, kernel, kernel)
    kernel_filter = kernel_filter.unsqueeze(dim=1)
    
    input = input.unsqueeze(dim=1).expand(input.shape[0], 8, input.shape[1],input.shape[2])
    
    contrast_depth = F.conv2d(input, weight=kernel_filter, groups=8)  # depthwise conv
    
    return contrast_depth

class Contrast_depth_loss(nn.Module):    # Pearson range [-1, 1] so if < 0, abs|loss| ; if >0, 1- loss
    def __init__(self):
        super(Contrast_depth_loss,self).__init__()
        return
    def forward(self, out, label): 
        '''
        compute contrast depth in both of (out, label),
        then get the loss of them
        tf.atrous_convd match tf-versions: 1.4
        '''
        contrast_out = contrast_depth_conv(out)
        contrast_label = contrast_depth_conv(label)
        
        criterion_MSE = nn.MSELoss().cuda()
    
        loss = criterion_MSE(contrast_out, contrast_label)
        #loss = torch.pow(contrast_out - contrast_label, 2)
        #loss = torch.mean(loss)
    
        return loss
def main():
    ## Dataset root ##
    train_list = "/home/minhtl/face_anti_spoof/Train.csv"
    val_list = "/home/minhtl/face_anti_spoof/Val.csv"

    args = get_arguments()

    # GPU  & log file  -->   if use DataParallel, please comment this command
    # os.environ["CUDA_VISIBLE_DEVICES"] = "%d, %d" % (args.gpu)
    os.environ["CUDA_VISIBLE_DEVICES"] = "0, 1"

    # device = torch.device(‘cuda’)
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')    

    isExists = os.path.exists(args.log)
    if not isExists:
        os.makedirs(args.log)
    log_file = open(args.log+'/'+ args.log+'_log.txt', 'w')
    save_dir = args.log + '/'
    # load the network, load the pre-trained model?
    log_file.write('train from scratch!\n')
    log_file.flush()

    # model = CDCNpp( basic_conv=Conv2d_cd, theta=0.7)
    model = nn.DataParallel(DC_CDN(basic_conv1=Conv2d_Hori_Veri_Cross, basic_conv2=Conv2d_Diag_Cross, theta=0.8))
    # model = nn.DataParallel(CDCNpp(basic_conv=Conv2d_cd, theta=args.theta))
    model = model.to(device)

    lr = args.lr

    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=0.00005)
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=args.step_size, gamma=args.gamma)
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            BEST_EER = checkpoint['best_eer']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})"
                    .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))
    else:
        BEST_EER = 1.0 
        print(BEST_EER)


    print(model)

    criterion_absolute = nn.MSELoss().to(device)
    criterion_contrastive  = Contrast_depth_loss().to(device)

    train_data = Spoofing_train(train_list, transform=transforms.Compose([RandomErasing(), RandomHorizontalFlip(), ToTensor(), Cutout(), Normaliztion()]))
    dataloader_train = DataLoader(train_data, batch_size=args.batchsize, shuffle=True, num_workers=8)
    val_data = Spoofing_valtest(val_list, transform=transforms.Compose([Normaliztion_valtest(), ToTensor_valtest()]))
    dataloader_val = DataLoader(val_data, batch_size=args.batchsize, shuffle=False, num_workers=8)

    #tensorboard
    comment = f"arch = {args.log}, batch_size = {args.batchsize},  lr = {args.lr}"
    writer = SummaryWriter(comment=comment)
    # images = next(iter(dataloader_train))
    # grid = torchvision.utils.make_grid(images)
    # writer.add_image("images", grid)
    # writer.add_graph(model, images)

    # ACER_save = 1.0
    for epoch in range(args.start_epoch, args.epochs):  # loop over the dataset multiple times
        scheduler.step()
        if (epoch + 1) % args.step_size == 0:
            lr *= args.gamma
        ## Train ##
        [loss_absolute_train, loss_contra_train, data_time, batch_time] = train(args.echo_batches, dataloader_train, model, criterion_absolute, criterion_contrastive,
                                                        optimizer, epoch, lr, device)
        
        print('epoch:%d, Train: Absolute_Depth_loss= %.4f, Contrastive_Depth_loss= %.4f, Overall_loss = %.4f, data_time = %.3f, batch_time = %.3f\n' % (epoch + 1, loss_absolute_train,     loss_contra_train, loss_absolute_train + loss_contra_train, data_time, batch_time))
        writer.add_scalar('Train/Loss', loss_absolute_train + loss_contra_train, epoch)
        writer.add_scalar('Train/Absolute_Loss', loss_absolute_train, epoch)
        writer.add_scalar('Train/Contractive_Loss', loss_contra_train, epoch)
        writer.add_scalar('Train/Learning rate',lr, epoch)
        log_file.write('epoch:%d, Train: Absolute_Depth_loss= %.4f, Contrastive_Depth_loss= %.4f, Overall_loss = %.4f, data_time = %.3f, batch_time = %.3f\n' % (epoch + 1, loss_absolute_train, loss_contra_train, loss_absolute_train + loss_contra_train, data_time, batch_time))
        log_file.flush()
        ## Val ##
        [loss_absolute_val, loss_contra_val, map_score_list, label_list, val_batch_time] = validate(dataloader_val, model, criterion_absolute, criterion_contrastive, device)

        ### Compute validate threshold    
        err_val, best_val_threshold, val_threshold_ACC, val_threshold_APCER, val_threshold_BPCER, val_threshold_ACER = performances_val(label_list, map_score_list)            
        if(err_val < BEST_EER):
            torch.save(model.state_dict(), args.log+'/'+args.log+'_%d.pkl' % (epoch + 1))
            BEST_EER = err_val
        ### Save checkpoint
        save_checkpoint(
            {'epoch' : epoch + 1,
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict(),
            'best_eer': BEST_EER,
            }, save_dir)

        print('epoch:%d, Val: Absolute_Depth_loss= %.4f, Contrastive_Depth_loss= %.4f, Overall_loss = %.4f, val_batch_time = %.3f\n' % (epoch + 1, loss_absolute_val, loss_contra_val,
                loss_absolute_val + loss_contra_val, val_batch_time))
        print("epoch:%d, Val:  val_threshold= %.4f, val_EER= %.4f, val_ACC= %.4f, val_APCER= %.4f,  val_BPCER= %.4f,  val_ACER= %.4f" % (epoch + 1, 
                best_val_threshold, err_val, val_threshold_ACC, val_threshold_APCER, val_threshold_BPCER, val_threshold_ACER))
        writer.add_scalar('Val/Loss', loss_absolute_val + loss_contra_val, epoch)
        writer.add_scalar('Val/Absolute_Loss', loss_absolute_val, epoch)
        writer.add_scalar('Val/Contractive_Loss', loss_contra_val, epoch)
        writer.add_scalar('Val/Acc@1', val_threshold_ACC, epoch)
        writer.add_scalar('Val/EER', err_val , epoch)
        writer.add_scalar('Val/ACER', val_threshold_ACER , epoch)

        log_file.write('epoch:%d, Train: Absolute_Depth_loss= %.4f, Contrastive_Depth_loss= %.4f, Overall_loss = %.4f, val_batch_time = %.3f\n' % (epoch + 1, loss_absolute_val, loss_contra_val, loss_absolute_val + loss_contra_val, val_batch_time))
        log_file.write("epoch:%d, Val:  val_threshold= %.4f, val_EER= %.4f, val_ACC= %.4f, val_APCER= %.4f,  val_BPCER= %.4f,  val_ACER= %.4f" % (epoch + 1, 
                        best_val_threshold, err_val, val_threshold_ACC, val_threshold_APCER, val_threshold_BPCER, val_threshold_ACER))
        log_file.flush()
            # save the model until the next improvement
    # writer.add_hparams(
    #     {"lr": lr, "bsize": batch_size},{"acc": "loss": losses},)
    torch.save(model.state_dict(), args.log+'/'+args.log+'_%d.pkl' % (epoch + 1))

    writer.flush()
    writer.close()

    print('Finished Training')
    log_file.close()

def train(echo_batches, dataloader_train, model, criterion_absolute, criterion_contrastive, optimizer, epoch, lr, device):
    loss_absolute = AvgrageMeter()
    loss_contra =  AvgrageMeter()
    data_time = AvgrageMeter()
    batch_time = AvgrageMeter()
    #top5 = utils.AvgrageMeter()

    model.train()
    end = time.time()
    for i, sample_batched in enumerate(dataloader_train):
        data_time.update(time.time() - end)
        # get the inputs
        inputs, map_label,  = sample_batched['image_x'].to(device), sample_batched['map_x'].to(device)
        spoof_label = sample_batched['spoofing_label'].to(device)
        # forward + backward + optimize
        # map_x, embedding, x_Block1, x_Block2, x_Block3, x_input =  model(inputs)
        map_x =  model(inputs)

        
        #pdb.set_trace()
        #pdb.set_trace()
        absolute_loss = criterion_absolute(map_x, map_label)
        contrastive_loss = criterion_contrastive(map_x, map_label)
        loss =  absolute_loss + contrastive_loss
        
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
        n = inputs.size(0)
        loss_absolute.update(absolute_loss.data, n)
        loss_contra.update(contrastive_loss.data, n)
        
        if i % echo_batches == echo_batches-1:    # print every 50 mini-batches            
            # visualization
            #FeatureMap2Heatmap(x_input, x_Block1, x_Block2, x_Block3, map_x)
            print('epoch:%d, mini-batch:%3d, lr=%f, Absolute_Depth_loss= %.4f, Contrastive_Depth_loss= %.4f' % (epoch + 1, i + 1, lr,  loss_absolute.avg, loss_contra.avg))

        batch_time.update(time.time() - end)         
        end = time.time()
    
    return  loss_absolute.avg, loss_contra.avg, data_time.avg, batch_time.avg 

def validate(dataloader_val, model, criterion_absolute, criterion_contrastive, device):
    loss_absolute = AvgrageMeter()
    loss_contra =  AvgrageMeter()  
    batch_time = AvgrageMeter()

    model.eval()
    with torch.no_grad():
        map_score_list = []
        label_list = []
        end = time.time()
        for i, sample_batched in enumerate(dataloader_val):
            inputs, spoofing_label = sample_batched['image_x'].to(device), sample_batched['spoofing_label'].to(device)
            val_map_x, val_map_standard = sample_batched['map_x'].to(device), sample_batched['map_standard'].to(device)
            
            # map_x, embedding, x_Block1, x_Block2, x_Block3, x_input =  model(inputs)
            map_x =  model(inputs)
            ## Compute loss val
            absolute_loss_val = criterion_absolute(map_x, val_map_x)
            contrastive_loss_val = criterion_contrastive(map_x, val_map_x)

            n = inputs.size(0)
            loss_absolute.update(absolute_loss_val.data, n)
            loss_contra.update(contrastive_loss_val.data, n)
            ## Compute map_score
            for j in range(len(inputs)):
                score_norm = torch.sum(map_x[j])/torch.sum(val_map_standard[j])
                if score_norm > 1:
                    score_norm = 1.0
                map_score_list.append(float(score_norm))
                label_list.append(float(spoofing_label[j]))

        batch_time.update(time.time() - end)
        end = time.time()

    return loss_absolute.avg, loss_contra.avg, map_score_list, label_list, batch_time.avg

def save_checkpoint(state, save_dir, filename='checkpoint.pth.tar'):
    torch.save(state, os.path.join(save_dir, filename))
    

  
if __name__ == '__main__':
    main()

  
 


